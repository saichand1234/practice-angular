import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// import {albumss} from '../services/album';

@Injectable({
  providedIn: 'root'
})

export class ServiceService {

  private _url: string = "https://jsonplaceholder.typicode.com";
private _json:string =  "assets/album.json";
  private _newMessage = new Subject<string>();
  newMessage$ = this._newMessage.asObservable();

  constructor(
    private http: HttpClient
  ) { }
  getAlbum(): Observable<any> {
    return this.http.get<any>(this._url + '/posts');

  }
  getUser(): Observable<any> {
    return this.http.get<any>(this._url + '/users');
  }
  sendMessage(message: string) {
    this._newMessage.next(message);
  }
  array():Observable<any>{
    return this.http.get<any>(this._json);
  }
}

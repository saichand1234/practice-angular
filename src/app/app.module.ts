import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { ServiceComponent } from './services/service/service.component';
import {HttpClientModule} from '@angular/common/http';
import { ServiceService } from './services/service.service';
import { MainComponent } from './core/main/main.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FeatureComponent } from './core/feature/feature.component';
import { LoginComponent } from './core/login/login.component';
import { CustomersComponent } from './core/customers/customers.component';
// import { CustomersCardComponent } from './core/main/customers-card/customers-card.component';
// import { CustomersGridComponent } from './core/main/customers-grid/customers-grid.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CustomersCardComponent } from './core/customers-card/customers-card.component';
import { CustomersGridComponent } from './core/customers-grid/customers-grid.component';
// import {NgxPaginationModule} from 'ngx-pagination';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { CardCustomerComponent } from './core/main/card-customer/card-customer.component';
import { GridCustomerComponent } from './core/main/grid-customer/grid-customer.component';
// import { AgmCoreModule } from '@agm/core';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    FeatureComponent,
    LoginComponent,
    CustomersComponent,
    CustomersCardComponent,
    CustomersGridComponent,
    CardCustomerComponent,
    GridCustomerComponent,
    // ServiceComponent,

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BsDropdownModule.forRoot(),
    TooltipModule,
    ModalModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    // NgxPaginationModule
    FilterPipeModule,
    // AgmCoreModule.forRoot({
    //   apiKey: ''
    // })
  ],
  providers: [ServiceService],
  bootstrap: [AppComponent]
  
})
export class AppModule { }

import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

interface CardsItems {
  userId: number;
  id: number;
  title: string;
  body: string;
}
@Component({
  selector: 'app-customers-card',
  templateUrl: './customers-card.component.html',
  styleUrls: ['./customers-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomersCardComponent implements OnInit {
  public card=[];
  public limititems;

  // @Input() card: ICustomer[] = [];
  // public eventspagination= [{itemsPerPage: 10, page: 2}]
  constructor(private _service: ServiceService) { }

  ngOnInit(): void {
    this._service.getAlbum()
      .subscribe(data => {
        this.card = data;
        console.log(data);
        this.limititems = this.card.slice(0,10);
      } 
        );
      console.log(this.card);
      
      
  }
  pageChanged(event: PageChangedEvent): void {
    console.log(event)
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.limititems = this.card.slice(startItem, endItem);
    console.log(this.limititems);
    console.log('Page changed to: ' + event.page);
    console.log('Number items per page: ' + event.itemsPerPage);
  }
  // sliceItem() {
  //   this.limititems = this.card;
  //   this.a = this.limititems.slice(0, 10);
  //   const cardss = this.limititems
  //   console.log(cardss);
  // }
  // totalItems: number = 65;
  // currentPage: number = 1;
  // smallnumPages: number = 0;

  // pageChanged(event: any): void {
  //   console.log('Page changed to: ' + event.page);
  //   console.log('Number items per page: ' + event.itemsPerPage);
  // }
}

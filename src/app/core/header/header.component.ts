import { Component, OnInit } from '@angular/core';
// import { Observable } from 'rxjs';
import { ServiceService } from '../../services/service.service';


interface KeyPair {
  key: number;
  value: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public num: number = 1;
  public myNumber: number = 123456;
  public values: (string | number)[] = ['Apple', 2, 'Orange', 3, 4, 'Banana'];
  public Print = () => console.log("Hello TypeScript");
  public kv1: KeyPair[] = [{ key: 1, value: "Steve" }, { key: 1, value: "Steve" }, { key: 1, value: "Steve" }];
  public listitem:(string)[] = ['Customers','Orders','About','Login'];
  itemImageUrl = '../assets/phone.png'
  public albums: any = [];

  constructor(
    private _service: ServiceService,
  ) { }

  ngOnInit(): void {
    this._service.getAlbum()
      .subscribe(data => this.albums = data);
      console.log(this.albums)
  }
}

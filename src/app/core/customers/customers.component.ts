import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ServiceService } from 'src/app/services/service.service';

// import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  public username: any;
  public users: any;
  public message;
  constructor(private _route: ActivatedRoute, private _service: ServiceService) { }

  ngOnInit(): void {
    console.log('thing', this._route.snapshot.params.id);

    // observable way
    this._route.paramMap.subscribe(params => {
      console.log(params.get('id'));
      this.username = params.get('id');
    });

    this._service.getUser()
      .subscribe(data => {
        this.users = data;
        console.log(data);
      }
      );
   
    this.zip();
  }
  title = 'My first AGM project';
  lat: any;
  lng = 7.809007;
  zip() {
    if (this.username == this.users.id) {
      this.lat = this.users.zipcode.lat;
      console.log(this.lat);
    }
  }
}




import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/services/service.service';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { CardCustomerComponent } from './card-customer/card-customer.component';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements AfterViewInit {

  @ViewChild(CardCustomerComponent) child;

  public form: FormGroup;
  displayMode: boolean = true;
  listMode: boolean = false;
  userFilter: any = { title: '' };
  public message: string = "Welcome to sharing data";
  public card = [];
  public limititems;
  public array = [];
  // public message:string= "abcfddddddddddddd";
  constructor(private fb: FormBuilder, private _router: Router, private _service: ServiceService) { }

  ngAfterViewInit(): void {
    this.message = this.child.message;
    console.log(this.child.message);

    this.form = this.fb.group({
      amount: new FormControl('', [Validators.required]),
      fromDate: new FormControl('', [Validators.required]),
      toDate: new FormControl('', [Validators.required]),
      rate: new FormControl('', [Validators.required])
    });
    this._service.getAlbum()
      .subscribe(data => {
        this.card = data;
        console.log(data);

        this.limititems = this.card.slice(0, 10);
      }
      );
    console.log(this.card);

    this._service.array().subscribe(info => {
      this.array = info;
      console.log(info);
      console.log(this.array);
      this.arraySort();

      this.array.sort((a, b) => {
        if (a.previous_stage_id === null || b.previous_stage_id === a.id) {
          return -1;
        }
        if (b.previous_stage_id === null || a.previous_stage_id === b.id) {
          return 1;
        }
        return 0;
      });
      console.log(this.array);
    });
    // this.interaction();
    

    // console.log(this.array);
  }
  onSubmit() {
    console.log(this.form.value);
    console.log(this.form.value.amount)
    if (this.form.valid) {
      this._router.navigateByUrl('feature')
    }
  }
  cardView() {
    this.displayMode = true;
    this.listMode = false;
  }
  listView() {
    this.displayMode = false;
    this.listMode = true;
  }
  pageChanged(event: PageChangedEvent): void {
    console.log(event)
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.limititems = this.card.slice(startItem, endItem);
    console.log(this.limititems);
    console.log('Page changed to: ' + event.page);
    console.log('Number items per page: ' + event.itemsPerPage);
  }
  receiveMessage($event) {
    this.message = $event;
  }
  interaction() {
    this._service.sendMessage("Hello This is saichand");
  }





arraySort(){
  console.log(this.array[1].id);
}






















}

import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';
// import { PageChangedEvent } from 'ngx-bootstrap/pagination';

// interface CardsItems {
//   userId: number;
//   id: number;
//   title: string;
//   body: string;
// }

@Component({
  selector: 'app-card-customer',
  templateUrl: './card-customer.component.html',
  styleUrls: ['./card-customer.component.scss']
})
export class CardCustomerComponent implements OnInit {
@Input() data;
  // public card=[];
  // public limititems;
public message:string= "abcd";
  // @Input() card: ICustomer[] = [];
  // public eventspagination= [{itemsPerPage: 10, page: 2}]
  constructor(private _service:ServiceService) { }

  ngOnInit(): void {
    // this._service.getAlbum()
    //   .subscribe(data => {
    //     this.card = data;
    //     console.log(data);
    //     this.limititems = this.card.slice(0,10);
    //   } 
    //     );
    //   console.log(this.card);
    this._service.newMessage$
    .subscribe(message => {
      this.message = message;
      console.log(message)
    })
      
  }
  // pageChanged(event: PageChangedEvent): void {
  //   console.log(event)
  //   const startItem = (event.page - 1) * event.itemsPerPage;
  //   const endItem = event.page * event.itemsPerPage;
  //   this.limititems = this.card.slice(startItem, endItem);
  //   console.log(this.limititems);
  //   console.log('Page changed to: ' + event.page);
  //   console.log('Number items per page: ' + event.itemsPerPage);
  // }

}

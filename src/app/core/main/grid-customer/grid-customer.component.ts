import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-grid-customer',
  templateUrl: './grid-customer.component.html',
  styleUrls: ['./grid-customer.component.scss']
})
export class GridCustomerComponent implements OnInit {
  @Input() data;
  
  constructor() { }

  ngOnInit(): void {
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersComponent } from './core/customers/customers.component';
import { FeatureComponent } from './core/feature/feature.component';
import { HeaderComponent } from './core/header/header.component';
import { LoginComponent } from './core/login/login.component';
import { MainComponent } from './core/main/main.component';

const routes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  {
    path: 'main',
    component: MainComponent,
  },
  {
    path: 'Login',
    component: LoginComponent,
  },
  {
    path: 'Customers/:id',
    component: CustomersComponent,
  },
  {
    path: 'feature',
    component: FeatureComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
